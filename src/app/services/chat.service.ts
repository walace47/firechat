import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import {Mensaje} from '../interfaces/interfaces';
import { map } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';


@Injectable({
  providedIn: 'root'
})
export class ChatService {

  private itemsCollection: AngularFirestoreCollection<Mensaje>;
  chats: Mensaje[];
  public usuario:any = {};

  constructor(private afs: AngularFirestore,
    public afAuth: AngularFireAuth)
  {
    this.afAuth.authState.subscribe(user => {
      if(user){
        this.usuario.name = user.displayName;
        this.usuario.uid = user.uid;
      }

    });
  }

  cargarMensajes(){
    this.itemsCollection = this.afs.collection<Mensaje>('chats',ref=> ref.orderBy('fecha','desc').limit(5));
    return this.itemsCollection.valueChanges().
    pipe(map((mensajes:Mensaje[])=>{
      this.chats = [];
      for (let mensaje of mensajes){
        this.chats.unshift(mensaje);
      }
      console.log(mensajes);
    }))

  }

  agregarMensaje(texto:string){
    let mensaje:Mensaje={
      emisor:this.usuario.name,
      fecha:new Date().getTime(),
      mensaje:texto,
      uid:this.usuario.uid

    };
    return this.itemsCollection.add(mensaje);

  }
  login() {
    this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider());

    /*this.afAuth.auth.signInWithEmailAndPassword('pedro@gmail.com', '223456').catch(function(error) {
  // Handle Errors here.
  var errorCode = error.code;
  var errorMessage = error.message;
  console.error(errorCode);
  // ...
});*/

//    this.afAuth.auth.signInWithPopup(new auth.EmailAuthProvider());
  }
  logout() {
    this.usuario = {};
    this.afAuth.auth.signOut();
  }
}

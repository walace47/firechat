export interface Mensaje{
  mensaje:string;
  emisor:string;
  fecha?:number;
  uid?:string;
}
